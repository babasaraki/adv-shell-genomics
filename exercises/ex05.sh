for sid in $(ls data/*.fastq.gz | cut -d"_" -f1 | sed "s:data/::" | sort | uniq)
do
    bash scripts/pipeline.sh $sid
done
